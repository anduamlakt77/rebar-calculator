import React, { useState } from "react";
import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";

export default function App() {
  const [length1, setLength1] = useState();
  const [length2, setLength2] = useState();
  const [quantity1, setQuantity1] = useState();
  const [quantity2, setQuantity2] = useState();

  function handleLength1Change(e) {
    e.preventDefault();
    setLength1(e.target.value);
  }
  function handleLength2Change(e) {
    e.preventDefault();
    setLength2(e.target.value);
  }
  function handleQuantity1Change(e) {
    e.preventDefault();
    setQuantity1(e.target.value);
  }
  function handleQuantity2Change(e) {
    e.preventDefault();
    setQuantity2(e.target.value);
  }

  function calculate_bars(length_one, quantity_one, length_two, quantity_two) {
    let rebar_length = 12;
    let cuts = [
      { width: length_one, count: quantity_one },
      { width: length_two, count: quantity_two },
    ];
    cuts = cuts.sort((a, b) => {
      return b.length - a.length;
    });
    let cut_remaining = quantity_one + quantity_two;
    let rebars = 0;
    while (cut_remaining > 0) {
      rebars++;
      let rebar = rebar_length;
      let rm = rebar;
      cut_remaining = 0;

      for (let i = 0; i < cuts.length; i++) {
        const cut = cuts[i];
        while (cut.count > 0 && rm - cut.width >= 0) {
          if (rm - cut.width >= 0) {
            rm = rm - cut.width;
            cut.count--;
          }
        }
        cut_remaining += cut.count;
      }
    }
    return rebars;
  }

  function calculate() {
    let length_one = length1;
    let quantity_one = quantity1;
    let length_two = length2;
    let quantity_two = quantity2;
    let required_rebars = calculate_bars(
      length_one,
      quantity_one,
      length_two,
      quantity_two
    );
    document.getElementById("result").innerHTML = required_rebars;
  }

  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        p: 2,
        border: "1px dashed grey",
      }}
    >
      <div>
        <FormControl sx={{ m: 1, width: "60ch" }} variant="outlined">
          <Grid container spacing={-12}>
            <Grid item xs={6} md={6}>
              <OutlinedInput
                type="number"
                id="length1"
                value={length1}
                onChange={handleLength1Change}
                endAdornment={
                  <InputAdornment position="end">mtr</InputAdornment>
                }
                aria-describedby="length1"
                inputProps={{
                  "aria-label": "length1",
                }}
              />
              <FormHelperText id="formLength1">Length 1</FormHelperText>
            </Grid>
            <Grid item xs={6} md={3}>
              <OutlinedInput
                type="number"
                id="quantity1"
                value={quantity1}
                onChange={handleQuantity1Change}
                aria-describedby="quantity1"
                inputProps={{
                  "aria-label": "quantity1",
                }}
              />
              <FormHelperText id="formQuantity1">Quantity 1</FormHelperText>
            </Grid>
          </Grid>

          <Grid container spacing={-12} style={{ paddingTop: 60 }}>
            <Grid item xs={6} md={6}>
              <OutlinedInput
                type="number"
                id="length2"
                value={length2}
                onChange={handleLength2Change}
                endAdornment={
                  <InputAdornment position="end">mtr</InputAdornment>
                }
                aria-describedby="length2"
                inputProps={{
                  "aria-label": "length2",
                }}
              />
              <FormHelperText id="formLength1">Length 2</FormHelperText>
            </Grid>

            <Grid item xs={6} md={3}>
              <OutlinedInput
                type="number"
                id="quantity2"
                value={quantity2}
                onChange={handleQuantity2Change}
                aria-describedby="quantity2"
                inputProps={{
                  "aria-label": "quantity2",
                }}
              />
              <FormHelperText id="formQuantity2">Quantity 2</FormHelperText>
            </Grid>
          </Grid>

          <Stack
            direction="row"
            spacing={2}
            style={{
              paddingTop: 25,
              paddingLeft: "20%",
            }}
          >
            <Button variant="contained" color="success" onClick={calculate}>
              Calculate
            </Button>
          </Stack>
        </FormControl>

        <p>Result:</p>
        <h4>
          Minimum Needed Rebar : <span id="result"></span>{" "}
        </h4>
      </div>
    </Box>
  );
}
